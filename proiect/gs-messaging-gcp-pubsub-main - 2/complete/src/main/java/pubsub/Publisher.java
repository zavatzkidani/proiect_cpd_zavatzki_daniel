package pubsub;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.Scanner;


public class Publisher {


    private static final String EXCHANGE_NAME = "topic_logs";

    public void startPublish() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqps://jrqfjcoz:asf3QPhbnX797t1k2_Z-t1hTQQMqJ-8_@baboon.rmq.cloudamqp.com/jrqfjcoz");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            channel.exchangeDeclare(EXCHANGE_NAME, "topic");

            System.out.print("\n<<topic>>: ");
            Scanner sc = new Scanner(System.in);
            String routingKey = sc.nextLine();
            System.out.print("\n<<payload>>: ");
            String message = sc.nextLine();

            if (routingKey.equals("sport") || routingKey.equals("gaming") || routingKey.equals("automotive")) {
                channel.basicPublish(EXCHANGE_NAME, routingKey, null, message.getBytes());
                System.out.println(" [x] Sent '" + routingKey + "':'" + message + "'");
            } else {
                System.out.print("You are not subscribed to " + routingKey);
            }

        }
    }
}