import org.springframework.boot.SpringApplication;
import pubsub.Subscriber;
import sockets.Client;
import sockets.Server;

import java.io.IOException;

public class Main {


    public static void main(String[] args) {

        Subscriber subscriber = new Subscriber();
        subscriber.start();

        Client client = new Client();
        client.start();

        Server server = new Server(client);

        try {
            server.start(8080);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
