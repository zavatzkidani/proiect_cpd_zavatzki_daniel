import pubsub.Subscriber;
import sockets.Client;
import sockets.Server;

import java.io.IOException;

public class Main {


    public static void main(String[] args) {

        Client client = new Client();
        client.start();

        Subscriber subscriber = new Subscriber();
        subscriber.start();

        Server server = new Server(client);

        try {
            server.start(8081);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
