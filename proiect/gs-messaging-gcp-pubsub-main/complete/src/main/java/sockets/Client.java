package sockets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class Client extends Thread{
    private Socket client;
    private PrintWriter out;
    private BufferedReader in;

    @Override
    public void run() {
        while (true) {
            try {
                startConnection("127.0.0.1", 8082);
                break;
            } catch (IOException e) {
                ;
            }
        }
    }

    public void startConnection(String host, int port) throws IOException {
        client = new Socket(host, port);
        out = new PrintWriter(client.getOutputStream(), true);
        sendMessage("token");
    }

    public void sendMessage(String msg) throws IOException {
        out = new PrintWriter(client.getOutputStream(), true);
        out.println(msg);
    }

    public void stopConnection() throws IOException {
        in.close();
        out.close();
        client.close();
    }

}