package pubsub;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.util.ArrayList;
import java.util.List;


public class Subscriber extends Thread{

    private static final String EXCHANGE_NAME = "topic_logs";

    @Override
    public void run() {
        try {
            subscribeToTopics();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void subscribeToTopics() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqps://jrqfjcoz:asf3QPhbnX797t1k2_Z-t1hTQQMqJ-8_@baboon.rmq.cloudamqp.com/jrqfjcoz");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String queueName = channel.queueDeclare().getQueue();

        List<String> topics = new ArrayList<>();
        topics.add("sport");
        topics.add("gaming");

        for (String bindingKey : topics) {
            channel.queueBind(queueName, EXCHANGE_NAME, bindingKey);
        }


        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println(" [x] Received '" +
                    delivery.getEnvelope().getRoutingKey() + "':'" + message + "'");
        };
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }
}