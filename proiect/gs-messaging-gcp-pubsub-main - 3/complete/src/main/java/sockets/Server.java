package sockets;

import pubsub.Publisher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class Server {
    private ServerSocket server;
    private Socket client;
    private PrintWriter out;
    private BufferedReader in;
    private final Client applicationClient;

    public Server(Client client) {
        this.applicationClient = client;
    }

    public void start(int port) throws IOException {
        server = new ServerSocket(port);
        client = server.accept();
        out = new PrintWriter(client.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(client.getInputStream()));

        while(true) {
            String message = in.readLine();
            if("token".equals(message)) {
                System.out.println("Token received.");
                System.out.println("Now you can write...");

                try {
                    Publisher publisher = new Publisher();
                    publisher.startPublish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                applicationClient.sendMessage(message);
                System.out.println("Token sent to another person");
                System.out.println("Waiting for token...");
            }

        }
    }


    public void stop() throws IOException {
        in.close();
        out.close();
        client.close();
        server.close();
    }
}